SpringBoot+Gradle多模块项目构建
===

本项目是一个基于Gradle项目构建工具构建的SpringBoot项目。

1. [Maven和Gradle对比](https://mp.weixin.qq.com/s/ORpwi2vebvAnTUG0sg4njA)

【腾讯文档】Gradle+SpringBoot构建多模块项目
https://docs.qq.com/doc/DSHpWbkxBSmt1cVpx

## structure

```
misas_codesec
├── eureka_server --微服务注册与发现中心
├── admin_server -- 微服务系统管理与调试
├── zuul_server -- 网关
├── zipkin_server -- 链路追踪与分析
├── common -- 公共，配置文件，脚手架等
├── dao -- 数据持久层
├── entity -- 实体类
├── code_platform -- 代码管理平台
├── engine_platform -- 引擎管理平台
└── analysis_platform -- 结果分析平台 
```

## tool

技术 | 说明 | 官网
----|----|----
Spring Boot | 容器 | [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
Spring WEBFlux | MVC 框架|
Spring Security | 认证和授权框架 | [https://spring.io/projects/spring-security](https://spring.io/projects/spring-security)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
MyBatisPlus |ORM框架补充 | https://mybatis.plus/
MyBatisGenerator | 数据层代码生成 | [http://www.mybatis.org/generator/index.html](http://www.mybatis.org/generator/index.html)
PageHelper | MyBatis物理分页插件 | [https://pagehelper.github.io/](https://pagehelper.github.io/)
Swagger-UI | 文档生产工具 | [https://github.com/swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui)
Hibernator-Validator | 验证框架 | [http://hibernate.org/validator/](http://hibernate.org/validator/)
Elasticsearch | 搜索引擎 | [https://github.com/elastic/elasticsearch](https://github.com/elastic/elasticsearch)
RabbitMq | 消息队列 | [https://www.rabbitmq.com/](https://www.rabbitmq.com/)
Redis | 分布式缓存 | [https://redis.io/](https://redis.io/)
MongoDb | NoSql数据库 | [https://www.mongodb.com/](https://www.mongodb.com/)
Docker | 应用容器引擎 | [https://www.docker.com/](https://www.docker.com/)
Druid | 数据库连接池 | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
JWT | JWT登录支持 | [https://github.com/jwtk/jjwt](https://github.com/jwtk/jjwt)
LogStash | 日志收集 | [https://github.com/logstash/logstash-logback-encoder](https://github.com/logstash/logstash-logback-encoder)
Lombok | 简化对象封装工具 | [https://github.com/rzwitserloot/lombok](https://github.com/rzwitserloot/lombok)
loc |代码行数统计 | https://github.com/cgag/loc

## 项目运行

### 打包
只能使用右侧的Gradle界面中找到app模块，才能打包，
![](./asset/img/bootJar.png)
![](./asset/img/springboot-jar.png)

去掉Project的`id 'io.spring.dependency-management' version '1.0.9.RELEASE'`就可以使用
```shell script
 ./gradlew bootJar -x test
```
正常打包了。
```groovy
plugins {
//    id 'org.springframework.boot' version '2.2.4.RELEASE'
    id 'io.spring.dependency-management' version '1.0.9.RELEASE'
    id 'java'
    id 'idea'
}
```

## 技术应用

## SpringBoot

1. 事件机制,程序内解耦.[SpringBootEventTest](./app/src/test/java/hxy/cupb/springboot/app/event/SpringBootEventTest.java)

## Mybatis-plus
1. IService的扩展使用
1. 逻辑删除
1. 枚举
1. BaseMapper的insert方法插入之后就会返回自增的id
## 缓存
1. [SpringBoot 使用 Caffeine 本地缓存](https://docs.qq.com/doc/DSE5iZ0ZXRVFLUWNk)
## Gradle
#### 多module构建
1. [Gradle多模块之父子模块](https://blog.csdn.net/YCJ_xiyang/article/details/96827905?utm_source=distribute.pc_relevant.none-task)

#### implementation和compile
我们项目中会遇到module引用的情况，比如现在项目中有app,module1,module2三个module,要在module1中依赖module2的代码，如下所示：
```groovy
 dependencies {     
  compile project(':module2')
  }
```
假设现在的需求是这样的，app依赖module1,module1依赖module2,那么在app内能调用module2的参数和方法吗？

如果是compile依赖的确实可以做到依赖传递，但是AS 3.0开始推荐使用implementation取代了compile，依赖传递失效了。
————————————————
版权声明：本文为CSDN博主「Gerry_Liang」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/LosingCarryJie/article/details/78946517

1. [gradle 新的依赖方式 implementation、api、compileOnly](https://blog.csdn.net/wangliblog/article/details/81366095?utm_source=distribute.pc_relevant.none-task)
#### 脚本
1. task
    1. 打印出各个模块名字
    ```groovy
    allprojects {
        task hello  { task -> println "I'm $task.project.name" }
    }
    ```
   执行命令 `./gradlew -q hello`
    ![](asset/img/allmodule.png)

#### 疑难杂症
1. [gradle 多模块Springboot项目 compile project引用其他模块的坑](https://blog.csdn.net/u012452555/article/details/81871063?utm_source=distribute.pc_relevant.none-task)
gradle项目有时需要小心依赖更新不及时的问题，实践过程中曾碰到自己库里面的class introspect failed的问题，google都是说第三方库compile配置问题，结果最后发现是自己的api更新了但是gradle没有拉下来导致的，清空gradle的缓存重新拉一下就好了。
本地调试时，如果是子模块项目，需要注意路径设置的问题，可能导致无法加载到资源

以上就是项目过程中遇到的一些配置问题，配置只是皮毛，看一遍大家都会，webflux的核心还是要把里面的响应式编程、对异步的支持给吃透，前路漫漫其修远兮，希望后面能有机会继续总结webflux核心原理吧。

作者：ohzyo
链接：https://www.jianshu.com/p/9b751eccd61a
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
# 尚未完成
1. 不能直接一下子打包所有模块。命令打包走不通。只能一个项目一个项目的打包。或者使用脚本，那样会重复打包公用模块。
# log
2020/02/21：完成app打包jar
2020/02/17: 完成多模块构建和mybatis-plus配置