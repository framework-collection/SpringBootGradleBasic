package hxy.cupb.springboot.app.controller;

import hxy.cupb.springboot.entity.UserEntity;
import hxy.cupb.springboot.entity.param.BasePageParam;
import hxy.cupb.springboot.entity.vo.BaseResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 用户控制器
 * @date 2020/2/17
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/get")
    public UserEntity getUser() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("不能透露密码");
        return userEntity;
    }

    @GetMapping("/list")
    public BaseResponseVO listUser(@Validated  BasePageParam basePageParam) {
        if (log.isDebugEnabled()) {
            log.debug("参数是：{}", basePageParam);
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("不能透露密码");
        return BaseResponseVO.success("用户信息", userEntity);
    }
}
