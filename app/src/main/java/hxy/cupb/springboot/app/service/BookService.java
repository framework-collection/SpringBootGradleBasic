package hxy.cupb.springboot.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hxy.cupb.springboot.model.BookModel;
import org.springframework.stereotype.Service;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description
 * @date 2020/2/29
 */
@Service
public interface BookService extends IService<BookModel> {
}
