package hxy.cupb.springboot.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hxy.cupb.springboot.entity.UserEntity;
import hxy.cupb.springboot.model.UserModel;
import org.springframework.stereotype.Service;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 用户服务接口
 * @date 2020/2/17
 */
@Service
public interface UserService extends IService<UserModel> {

    UserEntity login(UserEntity userEntity);
}
