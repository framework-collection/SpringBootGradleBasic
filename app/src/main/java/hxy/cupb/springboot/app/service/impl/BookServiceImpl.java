package hxy.cupb.springboot.app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hxy.cupb.springboot.app.service.BookService;
import hxy.cupb.springboot.mapper.BookMapper;
import hxy.cupb.springboot.model.BookModel;
import org.springframework.stereotype.Service;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description
 * @date 2020/2/29
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, BookModel> implements BookService {
}
