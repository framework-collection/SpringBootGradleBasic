package hxy.cupb.springboot.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hxy.cupb.springboot.app.service.UserService;
import hxy.cupb.springboot.entity.UserEntity;
import hxy.cupb.springboot.mapper.UserMapper;
import hxy.cupb.springboot.model.UserModel;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 用户服务接口实现类
 * @date 2020/2/17
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserModel> implements UserService {
    @Resource
    UserMapper userMapper;

    @Override
    public UserEntity login(UserEntity userEntity) {

        QueryWrapper<UserModel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tel", userEntity.getTel());
        UserModel userModel = userMapper.selectOne(queryWrapper);

        if (userEntity.getUserName().equals(userModel.getUserName())) {

        }

        return null;
    }
}
