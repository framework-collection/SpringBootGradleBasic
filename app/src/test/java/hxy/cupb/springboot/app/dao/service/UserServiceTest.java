package hxy.cupb.springboot.app.dao.service;

import hxy.cupb.springboot.app.BaseAppTest;
import hxy.cupb.springboot.app.service.UserService;
import hxy.cupb.springboot.entity.UserEntity;
import hxy.cupb.springboot.model.UserModel;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description UserService测试
 * @date 2020/2/22
 */
public class UserServiceTest extends BaseAppTest {
    @Resource
    UserService userService;

    @Test
    public void userService() {

        UserModel userModel = new UserModel();
        userModel.setTel("8010");
        userModel.setPassword("更下密码");
        userService.saveOrUpdate(userModel);
    }
}
