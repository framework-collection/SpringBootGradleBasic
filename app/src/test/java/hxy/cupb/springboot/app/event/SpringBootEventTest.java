package hxy.cupb.springboot.app.event;

import hxy.cupb.springboot.app.BaseAppTest;
import hxy.cupb.springboot.common.event.DemoPublisher;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description SpringBoot事件测试
 * @date 2020/2/22
 */
public class SpringBootEventTest extends BaseAppTest {
    @Autowired
    DemoPublisher demoPublisher;

    @Test
    public void publish() {
        demoPublisher.publish("hello application event");

    }

}
