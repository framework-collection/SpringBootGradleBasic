package hxy.cupb.springboot.app.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import hxy.cupb.springboot.app.BaseAppTest;
import hxy.cupb.springboot.entity.UserEntity;
import hxy.cupb.springboot.mapper.UserMapper;
import hxy.cupb.springboot.model.UserModel;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description UserMapper测试
 * @date 2020/2/22
 */
public class UserMapperTest extends BaseAppTest {
    @Resource
    UserMapper userMapper;

    @Test
    public void insert() {
        UserModel userModel = new UserModel();
//        userModel.setPassword("ssss");
        userModel.setUserName("我");
        userModel.setTel("89901");
        userMapper.insert(userModel);
        System.out.println("插入数据库之后的id:"+userModel.getId());
    }

    @Test
    public void insertExist() {
        UserModel userModel = new UserModel();
//        userModel.setPassword("ssss");e也米有用
//        由于id已经设置成自增,mybatisplus直接去掉了id!设置
        userModel.setId(49);
        userModel.setUserName("我更新下");
        userModel.setPassword("我更新下密码");
        userModel.setTel("8010");
        userMapper.insert(userModel);
    }

    @Test
    public void update() {

        UserModel userModel = new UserModel();
        userModel.setId(49);
        userModel.setPassword("ssss");
//        null值就不更新了
//        userModel.setUserName("我是");
        userMapper.updateById(userModel);
    }
    @Test
    public void selectOneTest(){
        String tel = "8010";
        userMapper.selectOne(new QueryWrapper<UserModel>().lambda().eq(UserModel::getTel,tel));
    }

    @Test
    public void selectByIdTest(){
        String id = "1";
        UserModel userModel = userMapper.selectById(id);
        System.out.println(userModel);
    }
    @Test
    public void selectAllTest(){
        List<UserModel> userModels = userMapper.selectList(null);
        List<UserModel> userModels1 = userMapper.selectList(null);
        userModels.forEach(System.out::println);
    }

    @Test
    public void deleteTest(){

    }
}
