package hxy.cupb.springboot.app.service.impl;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import hxy.cupb.springboot.app.BaseAppTest;
import hxy.cupb.springboot.app.service.BookService;
import hxy.cupb.springboot.mapper.BookMapper;
import hxy.cupb.springboot.model.BookModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description
 * @date 2020/2/29
 */
public class BookServiceImplTest extends BaseAppTest {
    @Autowired
    BookService bookService;

    @Resource
    BookMapper bookMapper;

    @Test
    public void tset(){
        List<BookModel> list = new ArrayList<>();
        BookModel bookModel =new BookModel();
        bookModel.setBookId(1);
        bookModel.setUserId(1);
        bookModel.setBookName("ss");
        list.add(bookModel);
        bookService.saveOrUpdateBatch(list);

    }

    @Test
    public void select(){
        BookModel bookModel =new BookModel();
        bookModel.setBookId(1);
        bookModel.setUserId(1);
        bookModel.setBookName("ss");

//        bookModel.selectById().sql().selectOne(" and user_id = ?",bookModel.getUserId());
        QueryWrapper<BookModel> bookModelQueryWrapper = new QueryWrapper<>();
        bookModelQueryWrapper.eq("book_id",bookModel.getBookId());
        bookModelQueryWrapper.eq("user_id",bookModel.getUserId());
        bookMapper.selectOne(bookModelQueryWrapper);
    }
}
