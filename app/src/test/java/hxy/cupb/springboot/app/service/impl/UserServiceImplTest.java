package hxy.cupb.springboot.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import hxy.cupb.springboot.app.BaseAppTest;
import hxy.cupb.springboot.app.service.UserService;
import hxy.cupb.springboot.model.UserModel;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description UserService测试
 * @date 2020/2/22
 */
public class UserServiceImplTest extends BaseAppTest {
    @Autowired
    UserService userService;

    @Test
    public void saveOrUpdate() {
        UserModel userModel = new UserModel();
        String tel = "8010";
        userModel.setTel(tel);
//        userModel.setPassword("我?的密码");
        userModel.setUserName("Chinses");
        userService.saveOrUpdate(userModel, new QueryWrapper<UserModel>().lambda().eq(UserModel::getTel, tel));
    }
}
