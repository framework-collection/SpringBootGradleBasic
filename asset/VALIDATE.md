### 参数检查
#### 1. 依赖
```groovy
       compile 'javax.validation:validation-api:2.0.0.Final'
```
```xml
   <!--jsr 303-->
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>1.1.0.Final</version>
        </dependency>
        <!-- hibernate validator-->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>5.2.0.Final</version>
        </dependency>
```
#### 2. 注解说明
  * @NotNull：不能为 null，但可以为 empty(""," "," ")
  * @NotEmpty：不能为 null，而且长度必须大于 0 (""," ")
  * @NotBlank：只能作用在 String 上，不能为 null，而且调用 trim() 后，长度必须大于 0("test") 即：必须有实际字符
![](./validate.jpeg)
#### 3. @Validated 声明要检查的参数
这里我们在控制器层进行注解声明
```java
     /**
         * 走参数校验注解
         * @param userDTO
         * @return
         */
        @PostMapping("/save/valid")

        public RspDTO save(@RequestBody @Validated UserDTO userDTO) {
            userService.save(userDTO);
            return RspDTO.success();
        }
```
#### 4. 对参数的字段进行注解标注
```java
    import lombok.Data;
    import org.hibernate.validator.constraints.Length;
    import javax.validation.constraints.*;
    import java.io.Serializable;
    import java.util.Date;
    /**
     * @author LiJing
     * @ClassName: UserDTO
     * @Description: 用户传输对象
     * @date 2019/7/30 13:55
     */
    @Data
    public class UserDTO implements Serializable {
        private static final long serialVersionUID = 1L;
        /*** 用户ID*/
        @NotNull(message = "用户id不能为空")
        private Long userId;
        /** 用户名*/
        @NotBlank(message = "用户名不能为空")
        @Length(max = 20, message = "用户名不能超过20个字符")
        @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9\\*]*$", message = "用户昵称限制：最多20字符，包含文字、字母和数字")
        private String username;
        /** 手机号*/
        @NotBlank(message = "手机号不能为空")
        @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
        private String mobile;
        /**性别*/
        private String sex;
        /** 邮箱*/
        @NotBlank(message = "联系邮箱不能为空")
        @Email(message = "邮箱格式不对")
        private String email;
        /** 密码*/
        private String password;
        /*** 创建时间 */
        @Future(message = "时间必须是将来时间")
        private Date createTime;
    }
```
#### 5. 在全局校验中增加校验异常
MethodArgumentNotValidException 是 springBoot 中进行绑定参数校验时的异常, 需要在 springBoot 中处理, 其他需要处理 ConstraintViolationException 异常进行处理.

为了优雅一点, 我们将参数异常, 业务异常, 统一做了一个全局异常, 将控制层的异常包装到我们自定义的异常中.

为了优雅一点, 我们还做了一个统一的结构体, 将请求的 code, 和 msg,data 一起统一封装到结构体中, 增加了代码的复用性.
```java
    import com.boot.lea.mybot.dto.RspDTO;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.dao.DuplicateKeyException;
    import org.springframework.web.bind.MethodArgumentNotValidException;
    import org.springframework.web.bind.annotation.ExceptionHandler;
    import org.springframework.web.bind.annotation.RestControllerAdvice;
    import org.springframework.web.servlet.NoHandlerFoundException;
    import javax.validation.ConstraintViolationException;
    import javax.validation.ValidationException;
    /**
     * @author LiJing
     * @ClassName: GlobalExceptionHandler
     * @Description: 全局异常处理器
     * @date 2019/7/30 13:57
     */
    @RestControllerAdvice
    public class GlobalExceptionHandler {
        private Logger logger = LoggerFactory.getLogger(getClass());
        private static int DUPLICATE_KEY_CODE = 1001;
        private static int PARAM_FAIL_CODE = 1002;
        private static int VALIDATION_CODE = 1003;

        /**
         * 处理自定义异常
         */
        @ExceptionHandler(BizException.class)
        public RspDTO handleRRException(BizException e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(e.getCode(), e.getMessage());
        }

        /**
         * 方法参数校验
         */
        @ExceptionHandler(MethodArgumentNotValidException.class)
        public RspDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(PARAM_FAIL_CODE, e.getBindingResult().getFieldError().getDefaultMessage());
        }

        /**
         * ValidationException
         */
        @ExceptionHandler(ValidationException.class)
        public RspDTO handleValidationException(ValidationException e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(VALIDATION_CODE, e.getCause().getMessage());
        }
        /**
         * ConstraintViolationException
         */
        @ExceptionHandler(ConstraintViolationException.class)
        public RspDTO handleConstraintViolationException(ConstraintViolationException e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(PARAM_FAIL_CODE, e.getMessage());
        }
        @ExceptionHandler(NoHandlerFoundException.class)
        public RspDTO handlerNoFoundException(Exception e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(404, "路径不存在，请检查路径是否正确");
        }
        @ExceptionHandler(DuplicateKeyException.class)
        public RspDTO handleDuplicateKeyException(DuplicateKeyException e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(DUPLICATE_KEY_CODE, "数据重复，请检查后提交");
        }
        @ExceptionHandler(Exception.class)
        public RspDTO handleException(Exception e) {
            logger.error(e.getMessage(), e);
            return new RspDTO(500, "系统繁忙,请稍后再试");
        }
    }

```
在 ValidationMessages.properties 就是校验的 message, 有着已经写好的默认的 message, 且是支持 i18n 的, 大家可以阅读源码赏析

## 自定义参数注解

#### 1. 比如我们来个 自定义身份证校验 注解
```java
    @Documented
    @Target({ElementType.PARAMETER, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = IdentityCardNumberValidator.class)
    public @interface IdentityCardNumber {
        String message() default "身份证号码不合法";
        Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
    }
```
这个注解是作用在 Field 字段上，运行时生效，触发的是 IdentityCardNumber 这个验证类。

   * message 定制化的提示信息，主要是从 ValidationMessages.properties 里提取，也可以依据实际情况进行定制
   * groups 这里主要进行将 validator 进行分类，不同的类 group 中会执行不同的 validator 操作
   * payload 主要是针对 bean 的，使用不多。
#### 2.  然后自定义 Validator
这个是真正进行验证的逻辑代码：
```java
    public class IdentityCardNumberValidator implements ConstraintValidator<IdentityCardNumber, Object> {
        @Override
        public void initialize(IdentityCardNumber identityCardNumber) {
        }
        @Override
        public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
            return IdCardValidatorUtils.isValidate18Idcard(o.toString());
        }
    }
```
IdCardValidatorUtils 在项目源码中, 可自行查看

#### 3. 使用自定义的注解
```java
        @NotBlank(message = "身份证号不能为空")
        @IdentityCardNumber(message = "身份证信息有误,请核对后提交")
        private String clientCardNo;
```
#### 4. 使用 groups 的校验

有的宝宝说同一个对象要复用, 比如 UserDTO 在更新时候要校验 userId, 在保存的时候不需要校验 userId, 在两种情况下都要校验 username, 那就用上 groups 了:
先定义 groups 的分组接口 Create 和 Update

```java
    import javax.validation.groups.Default;
    public interface Create extends Default {
    }
    import javax.validation.groups.Default;
    public interface Update extends Default{
    }
```
再在需要校验的地方 @Validated 声明校验组
```java
     /**
         * 走参数校验注解的 groups 组合校验
         *
         * @param userDTO
         * @return
         */
        @PostMapping("/update/groups")
        public RspDTO update(@RequestBody @Validated(Update.class) UserDTO userDTO) {
            userService.updateById(userDTO);
            return RspDTO.success();
        }
```
在 DTO 中的字段上定义好 groups = {} 的分组类型
```java
    @Data
    public class UserDTO implements Serializable {
        private static final long serialVersionUID = 1L;

        /*** 用户ID*/
        @NotNull(message = "用户id不能为空", groups = Update.class)
        private Long userId;

        /**
         * 用户名
         */
        @NotBlank(message = "用户名不能为空")
        @Length(max = 20, message = "用户名不能超过20个字符", groups = {Create.class, Update.class})
        @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9\\*]*$", message = "用户昵称限制：最多20字符，包含文字、字母和数字")
        private String username;
        /**
         * 手机号
         */
        @NotBlank(message = "手机号不能为空")
        @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误", groups = {Create.class, Update.class})
        private String mobile;
        /**
         * 性别
         */
        private String sex;
        /**
         * 邮箱
         */
        @NotBlank(message = "联系邮箱不能为空")
        @Email(message = "邮箱格式不对")
        private String email;
        /**
         * 密码
         */
        private String password;
        /*** 创建时间 */
        @Future(message = "时间必须是将来时间", groups = {Create.class})
        private Date createTime;
    }
```
注意: 在声明分组的时候尽量加上 extend javax.validation.groups.Default 否则, 在你声明 @Validated(Update.class) 的时候, 就会出现你在默认没添加 groups = {} 的时候的校验组 @Email(message = "邮箱格式不对"), 会不去校验, 因为默认的校验组是 groups = {Default.class}.

#### 5.restful 风格用法
在多个参数校验, 或者 @RequestParam 形式时候, 需要在 controller 上加注 @Validated
```java
     @GetMapping("/get")
        public RspDTO getUser(@RequestParam("userId") @NotNull(message = "用户id不能为空") Long userId) {
            User user = userService.selectById(userId);
            if (user == null) {
                return new RspDTO<User>().nonAbsent("用户不存在");
            }
            return new RspDTO<User>().success(user);
        }
    @RestController
    @RequestMapping("user/")
    @Validated
    public class UserController extends AbstractController {
    ....圣洛代码...
```
### 总结
用起来很简单, soEasy, 重点参与的统一结构体返回, 统一参数校验, 是减少我们代码大量的 try catch 的法宝, 我觉得在项目中, 将异常处理好, 并将异常做好日志管理, 才是很好的升华, 文章浅显, 只是一个菜鸟的进阶笔记….
这里只是个人见解, 技术菜, 欢迎大佬不吝赐教…