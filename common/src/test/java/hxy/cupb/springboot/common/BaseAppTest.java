package hxy.cupb.springboot.common;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 基础测试类
 * @date 2020/2/22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public abstract class BaseAppTest {
}
