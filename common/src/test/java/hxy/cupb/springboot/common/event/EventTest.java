package hxy.cupb.springboot.common.event;

import hxy.cupb.springboot.common.BaseAppTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 事件测试类
 * @date 2020/2/22
 */
public class EventTest extends BaseAppTest{

    @Autowired
    DemoPublisher demoPublisher;

    @Test
    public void publish() {
        demoPublisher.publish("hello application event");

    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EventTest.class);
        DemoPublisher demoPublisher = context.getBean(DemoPublisher.class);
        demoPublisher.publish("hello application event");
        context.close();
    }
}
