package hxy.cupb.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hxy.cupb.springboot.model.BookModel;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description
 * @date 2020/2/29
 */
public interface BookMapper extends BaseMapper<BookModel> {
}
