package hxy.cupb.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hxy.cupb.springboot.model.UserModel;

/**
 * @author eric
 */
public interface UserMapper extends BaseMapper<UserModel> {
}
