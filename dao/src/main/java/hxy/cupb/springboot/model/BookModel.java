package hxy.cupb.springboot.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 书籍
 * @date 2020/2/29
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BookModel extends Model {
    //    @TableId
    int userId;
    @TableId
//         联合主键不行 https://github.com/baomidou/mybatis-plus/issues/893
            int bookId;
    String bookName;
    int price;
    /**
     * 逻辑删除，配置了deleted字段为标记就不需要注解了
     */
    private Integer deleted;

}
