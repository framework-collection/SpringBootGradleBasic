package hxy.cupb.springboot.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author eric
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user")
public class UserModel extends Model {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("name")
    private String userName;
    @TableField("pwd")
    private String password;
    @TableField("tel")
    private String tel;
    @TableLogic
    private Integer deleted;
}
