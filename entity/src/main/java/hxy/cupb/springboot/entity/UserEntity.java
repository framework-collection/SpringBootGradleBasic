package hxy.cupb.springboot.entity;

import lombok.Data;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 用户实体类
 * @date 2020/2/17
 */
@Data
public class UserEntity {
    String userName;
    String tel;
    String rawPassword;

}
