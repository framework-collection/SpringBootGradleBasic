package hxy.cupb.springboot.entity.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author eric
 */
@Data
public class BasePageParam {

    /**
     * 页码
     */
    @NotBlank(message = "pageNum页号不可以为空")
    public String pageNum;

    /**
     * 每页大小
     */
    @NotBlank(message = "pageSize页面大小不可以为空")
    public String pageSize;

    public Integer getOffset() {
        return (this.getPageNum() - 1) * this.getPageSize();
    }


    public Integer getPageNum() {
        if (checkStrIsNum(pageNum)) {
            return Integer.parseInt(pageNum);
        } else {
            return 1;
        }
    }

    public Integer getPageSize() {

        if (checkStrIsNum(pageSize)) {
            return Integer.parseInt(pageSize);
        } else {
            return 10;
        }
    }


    /**
     * 利用Java的character.isDigit方法进行处理
     */
    public static boolean checkStrIsNum(String str) {
        if (str != null && str.length() > 0) {
            return false;
        } else {
            for (int i = 0; i < str.length(); i++) {
                System.out.println(str.charAt(i));
                if (!Character.isDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }
}
