package hxy.cupb.springboot.entity.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author eric
 * @program springboot-gradle-basic
 * @description 基础返回类
 * @date 2020/2/17
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BaseResponseVO<T> implements Serializable {
    private static final long serialVersionUID = 6610930573200798624L;
    /**
     * 时间戳
     */
    private long timestamp = System.currentTimeMillis();

    private String msg;
    private Integer code;
    private T data;

    BaseResponseVO() {
    }

    BaseResponseVO(Integer code) {
        this.code = code;
    }

    BaseResponseVO(String msg) {
        this.msg = msg;
    }


    public BaseResponseVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    BaseResponseVO(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    BaseResponseVO(String msg, T data) {
        this.msg = msg;
        this.data = data;
    }


    public static BaseResponseVO success() {
        return new BaseResponseVO(200);
    }

    public static BaseResponseVO success(String msg) {
        return new BaseResponseVO(200, msg);
    }

    public static <T> BaseResponseVO<T> success(String msg, T data) {
        return new BaseResponseVO(200, msg, data);
    }



//    public static <T> BaseResponseVO<T> success(T data) {
//        return new BaseResponseVO<T>(StatusCodeEnum.SUCCESS.getCode(), "success", data);
//    }
//
//    public static <T> BaseResponseVO<T> success(String message, T data) {
//        return new BaseResponseVO<T>(SUCCESS.getCode(), message, data);
//    }
//
//    public static <T> BaseResponseVO<T> error() {
//        return new BaseResponseVO<T>(FAIL.getCode(), FAIL.getDescription(), null);
//    }
//
//    public static <T> BaseResponseVO<T> error(String message) {
//        return new BaseResponseVO<T>(FAIL.getCode(), message, null);
//    }
//
//    public static <T> BaseResponseVO<T> error(T data) {
//        return new BaseResponseVO<T>(FAIL.getCode(), FAIL.getDescription(), data);
//    }
//
//    public static <T> BaseResponseVO<T> error(String message, T data) {
//        return new BaseResponseVO<T>(FAIL.getCode(), message, data);
//    }
//
//    public static <T> BaseResponseVO<T> notFound(String message, T data) {
//        return new BaseResponseVO<T>(DATA_NOT_FOUND.getCode(), message, data);
//    }
//
//    public static <T> BaseResponseVO<T> notFound(T data) {
//        return new BaseResponseVO<T>(DATA_NOT_FOUND.getCode(), DATA_NOT_FOUND.getDescription(), data);
//    }
//
//    public static <T> BaseResponseVO<T> deleteWarn(T data) {
//        return new BaseResponseVO<T>(DELETE_WARN.getCode(), DELETE_WARN.getDescription(), data);
//    }
//
//    /**
//     * 请求错误
//     *
//     * @param data
//     * @param <T>
//     * @return
//     */
//    public static <T> BaseResponseVO<T> badRequest(T data) {
//        return new BaseResponseVO<T>(PARAM_BASE_CHECK_FAIL.getCode(), PARAM_BASE_CHECK_FAIL.getDescription(), data);
//    }
//
//
//    public static <T> BaseResponseVO<T> badrequest() {
//        return new BaseResponseVO<T>(PARAM_BASE_CHECK_FAIL.getCode(), "no identifier arguments", null);
//    }
//
//    public static <T> BaseResponseVO<T> badrequest(String message) {
//        return new BaseResponseVO<T>(PARAM_BASE_CHECK_FAIL.getCode(), message, null);
//    }
//
//    public static <T> BaseResponseVO<T> badrequest(T data) {
//        return new BaseResponseVO<T>(PARAM_BASE_CHECK_FAIL.getCode(), "no identifier arguments", data);
//    }
//
//    public static <T> BaseResponseVO<T> badrequest(String message, T data) {
//        return new BaseResponseVO<T>(PARAM_BASE_CHECK_FAIL.getCode(), message, data);
//    }
//
//    public static <T> BaseResponseVO<T> noLogin(String message) {
//        return new BaseResponseVO<T>(UNAUTHORIZED.getCode(), message, null);
//    }
    
}
